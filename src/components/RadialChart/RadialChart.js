import React, { Component } from 'react'
import { withTheme } from 'styled-components'
import * as d3 from 'd3'


class RadialChart extends Component {
  render() {
    let { width, height, thickness, data, theme, colors, fontSize, bgColor, noStroke, inverted } = this.props
    const radius = this.props.radius || (width / 3.6)

    // Compute geometric angles
    const values = data.map(d => d.value)
    const angles = d3.pie().sort(null)(values)

    // Compute colors
    if (!colors) {
      colors = inverted ? theme.data.secondary : theme.data.primary
    }

    // Zip slice and angle data into single collection
    const sliceData = data.map((dataItem, i) => ({
      ...dataItem,
      ...angles[i],
      fill: dataItem.fill || colors[i % colors.length],
    }))

    // Compute geometrical paths
    let path = d3.arc()
      .outerRadius(radius - 10)

    const labelArc = d3.arc()
      .outerRadius(radius + 5)
      .innerRadius(radius + 5)

    // If thickness provided add innerRadius (radial chart instead of old-school pie)
    if (thickness) {
      path = path.innerRadius(radius - thickness)
    } else {
      path = path.innerRadius(0)
    }

    // Generate slices (SVG paths)
    const stroke = inverted ? theme.inverted.two[0] : theme.inverted.two[4]
    const arcProps = noStroke ? {} : { stroke, strokeWidth: 2 }
    const arcs = sliceData.map((slice, i) => (
      <path
        fill={slice.fill}
        key={i}
        d={path(slice)}
        {...arcProps}
      />
    ))

    // Generate labels (SVG texts)
    const labels = sliceData.map((g, i) => {
      if (g.value > 0) {
        const middleAngle = g.startAngle + (g.endAngle - g.startAngle) / 2.0
        const isLeft = (middleAngle > Math.PI)
        return (
          <text
            key={i}
            transform={`translate(${labelArc.centroid(g)})`}
            textAnchor={isLeft ? 'end' : 'start'}
            fontFamily="LL Circular Book Web"
            fontSize={fontSize}
            fill={inverted ? theme.inverted.one[0] : theme.one[0]}
          >
            {g.title && <title>{g.title}</title>}
            {g.label}
          </text>
        )
      } else {
        return null
      }
    })

    let bg
    if (bgColor) {
      bg = (<rect width="100%" height="100%" fill={bgColor} />)
    }

    // Wrap in SVG translating origin of chart to center of drawing
    return (
      <svg width={width} height={height}>
        {bg}
        <g transform={`translate(${width / 2},${height / 2})`}>
          <g>
            {arcs}
          </g>
          <g>
            {labels}
          </g>
        </g>
      </svg>
    )
  }

  static defaultProps = {
    inverted: false,
    width: 300,
    height: 300,
    fontSize: 14
  }
}

export default withTheme(RadialChart)
