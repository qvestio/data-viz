import React from 'react'
import { storiesOf } from '@storybook/react'
import { action } from '@storybook/addon-actions'
import styled from 'styled-components'

import RadialChart from './RadialChart'
import QuestionThemesRadialChart from './QuestionThemesRadialChart'
import { makeThemeColorScales } from '../../util/d3Utils'
import styledTheme from '../theme'


const Wrapper = styled.div`
  padding: 20px;
  ${({ dark, theme }) => dark ? `
    background-color: ${theme.inverted.two[0]};
  ` : ''}
`

const MOCK_DATA = [
  { value: 3, label: 'Other'  },
  { value: 5, label: 'How' },
  { value: 22, label: 'What et al.' },
  { value: 20, label: 'Who et al.' },
  { value: 10, label: 'Why' }
]

const MOCK_THEMES = [
  { themeId: 'e6d90c52-65ef-4d22-af0f-ad56d92c3884', name: 'Leadership', questionIds: ['8', '2', '3']  },
  { themeId: '72f63765-5891-45a6-9417-54000c2ab0a1', name: 'Strategy', questionIds: ['8', '2', '3']  },
  { themeId: '868cfcde-aea0-4472-8930-4ddd42fdc9d0', name: 'Collaboration', questionIds: ['8', '2', '3', '4', '5', '7', '9']  }
]

const colorScale = makeThemeColorScales(styledTheme, MOCK_THEMES)


storiesOf('Data Visualization/RadialChart', module)
  .add('Small', () => {
    return (
      <Wrapper>
        <RadialChart
          data={MOCK_DATA}
          onGroupClick={action('Group clicked')}
        />
      </Wrapper>
    )
  })
  .add('Medium', () => {
    return (
      <Wrapper>
        <RadialChart
          data={MOCK_DATA}
          onGroupClick={action('Group clicked')}
          height={450}
          width={450}
          radius={150}
        />
      </Wrapper>
    )
  })
  .add('Large', () => {
    return (
      <Wrapper>
        <RadialChart
          data={MOCK_DATA}
          onGroupClick={action('Group clicked')}
          height={600}
          width={600}
          radius={200}
        />
      </Wrapper>
    )
  })
  .add('Question themes', () => {
    return (
      <Wrapper>
        <QuestionThemesRadialChart
          width={500}
          height={320}
          themes={MOCK_THEMES}
          colors={colorScale}
          transparentBg={false}
        />
      </Wrapper>
    )
  })
  .add('Question themes (highlighted)', () => {
    const highlightedThemeId = MOCK_THEMES[0].themeId
    return (
      <Wrapper>
        <QuestionThemesRadialChart
          width={500}
          height={320}
          themes={MOCK_THEMES}
          colors={colorScale}
          highlighted={[highlightedThemeId]}
          transparentBg={false}
        />
      </Wrapper>
    )
  })
