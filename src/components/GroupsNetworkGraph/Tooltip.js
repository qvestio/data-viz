import React, { useEffect, useState } from 'react'
import PropTypes from 'prop-types'
import { usePopper } from 'react-popper'
import styled from 'styled-components'


const ComponentRoot = styled.div`
  background: ${({ theme }) => theme.one[0]};
  box-shadow: 10px 10px 14px 0 rgba(0,0,0,0.19);
  border-radius: 4px;
  padding: 20px;
  transition: 250ms opacity;
  pointer-events: none;
  font-family: ${({ theme }) => theme.font1};
  font-weight: 500;
  font-size: 14px;
  line-height: 26px;
  color: ${({ theme }) => theme.inverted.one[0]};

  & > * {
    display: block;
  }

  & > :first-child {
    margin-bottom: 20px;
  }

  & > :not(:last-child) {
    margin-bottom: 10px;
  }

  &[data-popper-placement^="right"] > [data-popper-arrow] {
    left: -4px;
  }

  &[data-popper-placement^="bottom"] > [data-popper-arrow] {
    top: -4px;
  }

  &[data-popper-placement^="left"] > [data-popper-arrow] {
    right: -6px;
  }

  &[data-popper-placement^="top"] > [data-popper-arrow] {
    bottom: -17px;
  }
`

const Arrow = styled.div`
  width: 10px;
  height: 10px;

  &:before {
    position: absolute;
    top: -4px;
    left: -4px;
    content: "";
    width: 15px;
    height: 15px;
    z-index: 10;
    background: ${({ theme }) => theme.one[0]};
    visibility: visible;
    transform: translateX(0px) rotate(45deg);
    transform-origin: center center;
  }
`

const OPTIONS = {
  placement: 'auto',
  strategy: 'absolute',
  modifiers: [
    {
      name: 'preventOverflow',
      options: {
        padding: 20
      },
    },
    {
      name: 'offset',
      options: {
        offset: [0, 20]
      },
    }
  ],
}

const virtualElm = {
  getBoundingClientRect: () => {
    return () => ({
      width: 0,
      height: 0,
      top: 0,
      right: 0,
      bottom: 0,
      left: 0,
    })
  }
}

const Tooltip = ({ children }) => {
  const [tipElm, setTipElm] = useState(null)
  const { styles, attributes, ...popperProps } = usePopper(virtualElm, tipElm, OPTIONS)

  useEffect(() => {
    if (tipElm == null) return
    const setMousePosition = ({ clientX, clientY }) => {
      virtualElm.getBoundingClientRect = () => ({
        width: 0,
        height: 0,
        top: clientY,
        right: clientX,
        bottom: clientY,
        left: clientX,
      })
      if (popperProps.update) popperProps.update()
    }
    document.addEventListener('mousemove', setMousePosition)
    return () => document.removeEventListener('mousemove', setMousePosition)
  }, [tipElm, popperProps.update])

  return (
    <ComponentRoot ref={setTipElm} style={styles.popper} {...attributes.popper}>
      <Arrow data-popper-arrow style={styles.arrow} />
      {children}
    </ComponentRoot>
  )
}

Tooltip.propTypes = {
  centered: PropTypes.bool,
  target: PropTypes.any.isRequired,
}

Tooltip.defaultProps = {
  centered: false,
}

export default Tooltip
