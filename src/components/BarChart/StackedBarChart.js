import React, { Component } from 'react'
import PropTypes from 'prop-types'
import styled, { withTheme } from 'styled-components'
import * as d3 from 'd3'

import HorizontalTickAxis from './HorizontalTickAxis'
import HorizontalAxisTitle from './HorizontalAxisTitle'
import AxisLabel from './AxisLabel'
import Bar from './Bar'
import BarLabel from './BarLabel'


// Hover styling done in CSS, remaining styling done in-line for portability (i.e. download or print)
const LabelGroup = styled.g`  
  &:hover {
    & text {
      user-select: none;
      font-weight: bold;
      opacity: 1.0
    }
    ${({ hoverFill }) => hoverFill.map((color, i) => (`
      & > g:nth-child(${i+1}) rect {
        fill: ${color};
      }
    `))}
  }
`


const LEFT_PADDING = 60
const RIGHT_PADDING = 12
const BOTTOM_PADDING = 42
const DATA_Y_START_PADDING = 30
const DATA_X_START_PADDING = 25
const BAR_HEIGHT = 25
const BAR_GAP = 12


class StackedBarChart extends Component {
  render() {
    let { data, width, colors, theme, valuesTitle, unitTitle } = this.props

    const { labels, values } = data
    const ids = data.values.map(v => v.id)
    const labelIds = Object.keys(labels)
    colors = colors || theme.data

    const yAxisHeight = (labelIds.length * (BAR_HEIGHT + BAR_GAP)) + (2 * DATA_Y_START_PADDING)
    const xAxisWidth = (width - LEFT_PADDING - DATA_X_START_PADDING - RIGHT_PADDING)
    const height = yAxisHeight + BOTTOM_PADDING

    // Pivot data (Needed to utilize d3 stack function)
    const input = labelIds.map(() => new Object())
    values.forEach(item =>
      labelIds.forEach((labelId, i) =>
        input[i][item.id] = item[labelId]
      )
    )

    // Prepare stacks
    const stack = d3.stack().keys(ids)
    const stackSeries = stack(input)

    // Find maximum X-value
    const last = stackSeries[stackSeries.length-1]
    const endValues = last.map(item => item[1])
    const maxX = Math.round(d3.max(endValues))

    // Vertical draw scale (id -> y-coordinate)
    const scaleY = d3.scalePoint()
      .range([0, yAxisHeight])
      .padding(DATA_Y_START_PADDING)
      .domain(labelIds)

    // Horizontal draw scale 1 (value -> x-coordinate)
    const scaleX = d3.scaleLinear()
      .range([0, xAxisWidth])
      .domain([0, maxX])
      .nice(10)

    // Text label scale (id -> text label)
    const scaleLabel = d3.scaleOrdinal()
      .range(Object.values(labels))
      .domain(labelIds)

    // Primary color scale (id -> color)
    const scaleColor1 = d3.scaleOrdinal()
      .range(colors.primary)
      .domain(labelIds)

    // Secondary color scale (id -> color)
    const scaleColor2 = d3.scaleOrdinal()
      .range(colors.secondary)
      .domain(labelIds)

    // Labels
    const yLabels = scaleY.domain().map(id => (
      <AxisLabel key={id} x={0} y={scaleY(id)} value={scaleLabel(id)} />
    ))

    // Bars
    const bars = stackSeries.map((segments, i) => {
      const item = values[i]
      return (
        <LabelGroup key={segments.key} hoverFill={scaleColor2.range()}>
          {segments.map((segment, i) => {
            if (!item[labelIds[i]]) return (<g/>)  // If value is missing or zero, return empty group
            return (
              <g key={i}>
                <BarLabel
                  isBelow={i % 2 > 0}
                  x1={scaleX(segment[0])}
                  x2={scaleX(segment[1])}
                  y={scaleY(labelIds[i])}
                  value={item.name}
                />
                <Bar
                  value={item[labelIds[i]]}
                  x1={scaleX(segment[0])}
                  x2={scaleX(segment[1])}
                  y={scaleY(labelIds[i])}
                  fill={scaleColor1(labelIds[i])}
                  stroke="white"
                  height={BAR_HEIGHT}
                  formatText={v => `${v}%`}
                  hasCenteredText={true}
                />
              </g>
            )
          })}
        </LabelGroup>
      )
    })


    // Compose SVG element
    return (
      <div>
        <svg width={width} height={height}>
          {/* Vertical labels */}
          <g transform={`translate(${LEFT_PADDING}, 0)`}>
            {yLabels}
          </g>
          {/* X-axis */}
          <g transform={`translate(${LEFT_PADDING + DATA_X_START_PADDING}, ${yAxisHeight})`}>
            <HorizontalTickAxis scale={scaleX} tickCount={10} />
            <HorizontalAxisTitle scale={scaleX} title={valuesTitle} subtitle={unitTitle} />
          </g>
          {/* Bars */}
          <g transform={`translate(${LEFT_PADDING + DATA_X_START_PADDING}, 0)`}>
            {bars}
          </g>
        </svg>
      </div>
    )
  }
}

StackedBarChart.propTypes = {
  valuesTitle: PropTypes.string,
  unitTitle: PropTypes.string,
  isLoading: PropTypes.bool,
  data: PropTypes.shape({
    ids: PropTypes.arrayOf(PropTypes.string),
    labels: PropTypes.arrayOf(PropTypes.string),
    values1: PropTypes.arrayOf(PropTypes.number),
    values2: PropTypes.arrayOf(PropTypes.number)
  }),
  colors: PropTypes.shape({
    primary: PropTypes.arrayOf(PropTypes.string),
    secondary: PropTypes.arrayOf(PropTypes.string)
  })
}

StackedBarChart.defaultProps = {
  isLoading: false,
  valuesTitle: '',
  unitTitle: ''
}

export default withTheme(StackedBarChart)
