import React, { Component } from 'react'
import PropTypes from 'prop-types'
import styled, { withTheme } from 'styled-components'
import * as d3 from 'd3'
import zip from 'lodash/zip'

import HorizontalTickAxis from './HorizontalTickAxis'
import HorizontalAxisTitle from './HorizontalAxisTitle'
import AxisLabel from './AxisLabel'
import Bar from './Bar'


// Hover styling done in CSS, remaining styling done in-line for portability (i.e. download or print)
const LabelGroup = styled.g`  
  &:hover {
    & text {
      user-select: none;
      font-weight: bold;
      opacity: 1.0
    }
    & > g:nth-child(2) rect {
      fill: ${({ theme }) => theme.data.secondary[0]};
    }
    & > g:nth-child(3) rect {
      fill: ${({ theme }) => theme.data.secondary[3]};
    }
  }
`


const LEFT_PADDING = 90
const RIGHT_PADDING = 12
const BOTTOM_PADDING = 42
const DATA_Y_START_PADDING = 20
const DATA_X_START_PADDING = 25
const MIDDLE_GAP = 30
const BAR_HEIGHT = 25
const BAR_GAP = 12
const TICK_COUNT = 4


const VerticalSeparator = ({ height }) => (
  <path
    stroke="#E5E5E5"
    strokeWidth="5"
    strokeLinecap="round"
    d={`M0,0 L0,${height}`}
  />
)


class ButterflyBarChart extends Component {
  render() {
    let { data, theme, width } = this.props
    const { ids, labels, values1, values2 } = data

    const yAxisHeight = (data.labels.length * (BAR_HEIGHT + BAR_GAP)) + (2 * DATA_Y_START_PADDING)
    const xAxisWidth = (width - LEFT_PADDING - MIDDLE_GAP - DATA_X_START_PADDING - RIGHT_PADDING) / 2
    const height = yAxisHeight + BOTTOM_PADDING

    // Text label scale (id -> text label)
    const scaleLabel = d3.scaleOrdinal()
      .range(labels)
      .domain(ids)

    // Vertical draw scale (id -> y-coordinate)
    const scaleY = d3.scalePoint()
      .range([0, yAxisHeight])
      .padding(DATA_Y_START_PADDING)
      .domain(ids)

    // Horizontal draw scale 1 (value -> x-coordinate)
    const maxX = d3.max(values1.concat(values2))
    const scaleX1 = d3.scaleLinear()
      .range([0, xAxisWidth])
      .domain([0, maxX])
      .nice(TICK_COUNT)

    // Horizontal draw scale 2 (value -> x-coordinate)
    const scaleX2 = d3.scaleLinear()
      .range([xAxisWidth, 0]) // Reversed/mirrored horizontally
      .domain([0, maxX])
      .nice(TICK_COUNT)

    // Labels
    const yMarkers = scaleY.domain().map(id => (
      <AxisLabel
        key={id}
        x={0}
        y={scaleY(id)}
        maxWidth={LEFT_PADDING}
        value={scaleLabel(id)}
      />
    ))

    // Bars
    function generateBars(scaleX, scaleY, values, ids, fill) {
      const originPx = scaleX(d3.min(scaleX.range()))
      const items = zip(values, ids)
      return items.map(([value, id]) => {
        return (
          <Bar
            key={id}
            value={value}
            x1={originPx}
            x2={scaleX(value)}
            y={scaleY(id)}
            fill={fill}
            height={BAR_HEIGHT}
          />
        )
      })
    }
    const bars1 = generateBars(scaleX2, scaleY, values1, ids, theme.data.primary[0])
    const bars2 = generateBars(scaleX1, scaleY, values2, ids, theme.data.primary[3])

    // Group labels and bars to allow shared hover-styling
    const itemElements = zip(ids, yMarkers, bars1, bars2)
    const itemGroups = itemElements.map(([id, marker, bar1, bar2]) => (
      <LabelGroup key={id}>
        <g>
          {marker}
        </g>
        <g transform={`translate(${DATA_X_START_PADDING}, 0)`}>
          {bar1}
        </g>
        <g transform={`translate(${DATA_X_START_PADDING + xAxisWidth + MIDDLE_GAP}, 0)`}>
          {bar2}
        </g>
      </LabelGroup>
    ))

    // Compose SVG element
    return (
      <div>
        <svg width={width} height={height}>
          {/* Labels + Bars */ }
          <g transform={`translate(${LEFT_PADDING}, 0)`}>
            {itemGroups}
          </g>
          {/* X-axis 1 (left) */}
          <g transform={`translate(${LEFT_PADDING + DATA_X_START_PADDING}, ${yAxisHeight})`}>
            <HorizontalTickAxis scale={scaleX2} tickCount={TICK_COUNT} />
            <HorizontalAxisTitle scale={scaleX2} title="Questions" subtitle="(asked per participant)" />
          </g>
          {/* X-axis 2 (right) */}
          <g transform={`translate(${LEFT_PADDING + DATA_X_START_PADDING + xAxisWidth + MIDDLE_GAP}, ${yAxisHeight})`}>
            <HorizontalTickAxis scale={scaleX1} tickCount={TICK_COUNT} />
            <HorizontalAxisTitle scale={scaleX1} title="Answers" subtitle="(given per participant)" />
          </g>
          <g transform={`translate(${LEFT_PADDING + DATA_X_START_PADDING + xAxisWidth + (MIDDLE_GAP / 2)}, ${DATA_Y_START_PADDING})`}>
            <VerticalSeparator height={yAxisHeight - (2 * DATA_Y_START_PADDING)} />
          </g>
        </svg>
      </div>
    )
  }
}

ButterflyBarChart.propTypes = {
  isLoading: PropTypes.bool,
  data: PropTypes.shape({
    ids: PropTypes.arrayOf(PropTypes.string),
    labels: PropTypes.arrayOf(PropTypes.string),
    values1: PropTypes.arrayOf(PropTypes.number),
    values2: PropTypes.arrayOf(PropTypes.number)
  })
}

ButterflyBarChart.defaultProps = {
  isLoading: false
}

export default withTheme(ButterflyBarChart)
