import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { withTheme } from 'styled-components'
import { truncateText } from '../../util/svgUtils'


class AxisLabel extends Component {
  componentDidMount() {
    // Truncate, if needed
    const { maxWidth } = this.props
    if (maxWidth) {
      truncateText(this.elm, maxWidth)
    }
  }

  render() {
    const { x, y, value, theme } = this.props
    return (
      <g transform={`translate(${x},${y})`}>
        <text
          ref={elm => this.elm = elm}
          fontFamily={theme.font1}
          fontWeight="500"
          fontSize="13"
          fill={theme.default}
          x="10"
          dy="0.32em"
          textAnchor="end"
          data-text={value}
        >
          {value}
        </text>
      </g>
    )
  }
}

AxisLabel.propTypes = {
  x: PropTypes.number.isRequired,
  y: PropTypes.number.isRequired,
  value: PropTypes.oneOfType([
    PropTypes.number,
    PropTypes.string
  ]).isRequired,
  maxWidth: PropTypes.number
}

export default withTheme(AxisLabel)
