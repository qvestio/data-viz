import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { withTheme } from 'styled-components'
import { truncateText } from '../../util/svgUtils'

const PADDING = 10

class BarLabel extends Component {
  constructor(props) {
    super(props)
    this.handleResize = this.handleResize.bind(this)
  }

  handleResize() {
    const { mayTruncate, x1, x2 } = this.props
    if (mayTruncate) {
      const maxWidth = (x2 - x1) - PADDING
      truncateText(this.elm, maxWidth)
    }
  }

  componentDidMount() {
    window.addEventListener('resize', this.handleResize)
    this.handleResize()
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.handleResize)
  }

  render() {
    const { theme, value, x1, y, isBelow } = this.props
    const labelOffset = (isBelow ? 25 : -18)
    const attr = {
      ref: elm => this.elm = elm,
      x: x1 + 2,
      y: Math.ceil(y + labelOffset),
      fill: theme.default,
      fontFamily: theme.font2,
      fontSize: 10,
      fontWeight: 500,
      textAnchor: 'start'
    }
    return (
      <text data-text={value} {...attr}>{value}</text>
    )
  }
}

BarLabel.propTypes = {
  x1: PropTypes.number.isRequired,
  x2: PropTypes.number.isRequired,
  y: PropTypes.number.isRequired,
  value: PropTypes.string.isRequired,
  mayTruncate: PropTypes.bool,
  isBelow: PropTypes.bool
}

BarLabel.defaultProps = {
  isBelow: false,
  mayTruncate: true
}

export default withTheme(BarLabel)
