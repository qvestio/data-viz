import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { withTheme } from 'styled-components'

const labelPadding = 6
function textPosition(width, hasCenteredText, isFlipped) {
  if (hasCenteredText) {
    return {
      dx: (width / 2),
      textAnchor: 'middle'
    }
  }
  if (isFlipped) {
    return {
      dx: labelPadding,
      textAnchor: 'begin'
    }
  }
  return {
    dx: width - labelPadding,
    textAnchor: 'end'
  }
}

class Bar extends Component {
  constructor(props) {
    super(props)
    this.strokeWidth = 3
  }
  render() {
    const { value, x1, x2, y, fill, stroke, height, formatText, hasCenteredText, theme } = this.props
    let strokeAttr = {}
    if (stroke) {
      strokeAttr = {
        stroke: stroke,
        strokeWidth: this.strokeWidth
      }
    }
    const inHeight = stroke ? (height + this.strokeWidth) : height
    const attr = {
      width: Math.abs(x1 - x2),
      height: inHeight,
      x: Math.ceil(Math.min(x1, x2)),
      y: Math.ceil(y - (inHeight / 2)),
      rx: stroke ? '4' : '2',
      fill,
      ...strokeAttr
    }
    const isFlipped = (x1 > x2)
    const { dx, textAnchor } = textPosition(attr.width, hasCenteredText, isFlipped)
    const textAttr = {
      x: Math.ceil(attr.x + dx),
      y: Math.ceil(attr.y + (attr.height / 2)),
      fill: 'white',
      fontFamily: theme.font4,
      fontWeight: 500,
      fontSize: 13,
      opacity: 0,
      dominantBaseline: 'middle',
      textAnchor
    }
    return (
      <g>
        <rect {...attr} />
        <text {...textAttr} >{formatText(value)}</text>
      </g>
    )
  }
}

Bar.propTypes = {
  x1: PropTypes.number.isRequired,
  x2: PropTypes.number.isRequired,
  y: PropTypes.number.isRequired,
  value: PropTypes.oneOfType([
    PropTypes.number,
    PropTypes.string
  ]).isRequired,
  fill: PropTypes.string,
  stroke: PropTypes.string,
  height: PropTypes.number,
  formatText: PropTypes.func,
  hasCenteredText: PropTypes.bool
}

Bar.defaultProps = {
  fill: 'black',
  height: 25,
  formatText: v => v,
  hasCenteredText: false
}

export default withTheme(Bar)
