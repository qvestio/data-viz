import React from 'react'
import PropTypes from 'prop-types'
import { withTheme } from 'styled-components'


const CircleTicks = ({ theme, inverted,  origin, scales, tickCount, hidden }) => {
  if (hidden) return null

  const labelPadding = 5
  const ticks = scales.north.ticks(5).slice(1, tickCount+1)
  const attrs = {
    cx: origin.x,
    cy: origin.y,
    fill: 'none',
    stroke: inverted ? theme.inverted.one[1] : theme.one[0],
    strokeWidth: '1px',
    strokeOpacity: '0.1'
  }
  const circles = ticks.map((tick, i) => (
    <circle
      {...attrs}
      key={i}
      r={origin.y - scales.north(tick)}
    />
  ))
  const labels = ticks.map((tick, i) => (
    <text
      key={i}
      x={origin.x - labelPadding}
      y={scales.north(tick) + labelPadding}
      textAnchor="end"
      alignmentBaseline="hanging"
      fontFamily={theme.font2}
      fontWeight="300"
      fontSize="10px"
      fill={theme.default}
      fillOpacity="0.3"
    >
      {tick * 100}
    </text>
  ))
  return (
    <g>
      <g>{labels}</g>
      <g>{circles}</g>
    </g>
  )
}

CircleTicks.propTypes = {
  scales: PropTypes.shape({
    north: PropTypes.func.isRequired,
    east: PropTypes.func.isRequired,
    south: PropTypes.func.isRequired,
    west: PropTypes.func.isRequired,
  }).isRequired,
  origin: PropTypes.shape({
    x: PropTypes.number.isRequired,
    y: PropTypes.number.isRequired
  }).isRequired,
  tickCount: PropTypes.number,
  hidden: PropTypes.bool,
  inverted: PropTypes.bool
}

CircleTicks.defaultProps = {
  tickCount: 10,
  hidden: false,
  inverted: false
}

export default withTheme(CircleTicks)
