import React from 'react'
import styled from 'styled-components'
import { storiesOf } from '@storybook/react'

import PolarChart from './PolarChart'

const MOCK_DATA = {
  north: { value: 0.10, label: 'Why' },
  east: { value: 0.22, label: 'What.'},
  south: { value: 0.20, label: 'Who'},
  west: { value: 0.05, label: 'How'}
}

const Wrapper = styled.div`
  padding: 20px;
  width: 100vw;
  height: 100vh;
  max-height: 100vh;
`

storiesOf('Data Visualization/PolarChart', module)
  .add('3 Points - East', () => {
    const show = { north: true, east: true, south: true }
    return (
      <Wrapper>
        <PolarChart
          primary
          data={MOCK_DATA}
          show={show}
          sum
        />
      </Wrapper>
    )
  })
  .add('3 Points - West', () => {
    const show = { north: true, west: true, south: true }
    return (
      <Wrapper>
        <PolarChart
          primary
          data={MOCK_DATA}
          show={show}
          sum
        />
      </Wrapper>
    )
  })
  .add('2 Points - North-east', () => {
    const show = { north: true, east: true }
    return (
      <Wrapper>
        <PolarChart
          primary
          data={MOCK_DATA}
          show={show}
          sum
        />
      </Wrapper>
    )
  })
  .add('2 Points - South-east', () => {
    const show = { south: true, east: true }
    return (
      <Wrapper>
        <PolarChart
          primary
          data={MOCK_DATA}
          show={show}
          sum
        />
      </Wrapper>
    )
  })
  .add('2 Points - South-west', () => {
    const show = { south: true, west: true }
    return (
      <Wrapper>
        <PolarChart
          primary
          data={MOCK_DATA}
          show={show}
          sum
        />
      </Wrapper>
    )
  })
  .add('2 Points - North-west', () => {
    const show = { north: true, west: true }
    return (
      <Wrapper>
        <PolarChart
          primary
          data={MOCK_DATA}
          show={show}
          sum
        />
      </Wrapper>
    )
  })
  .add('Zero value', () => {
    const show = { north: true, west: true, south: true }
    const data = {
      ...MOCK_DATA,
      west: { value: 0, label: 'Why' }
    }
    return (
      <Wrapper>
        <PolarChart
          primary
          data={data}
          show={show}
          sum
        />
      </Wrapper>
    )
  })
  .add('All zero values', () => {
    const show = { north: true, west: true, south: true }
    const data = {
      north: { value: 0, label: 'Why' },
      east: { value: 0, label: 'What.'},
      south: { value: 0, label: 'Who'},
      west: { value: 0, label: 'How'}
    }
    return (
      <Wrapper>
        <PolarChart
          primary
          data={data}
          show={show}
          sum
        />
      </Wrapper>
    )
  })
  .add('All zero values (round)', () => {
    const data = {
      north: { value: 0, label: 'Why' },
      east: { value: 0, label: 'What.'},
      south: { value: 0, label: 'Who'},
      west: { value: 0, label: 'How'}
    }
    return (
      <Wrapper>
        <PolarChart
          primary
          data={data}
          sum
          round
        />
      </Wrapper>
    )
  })
  .add('Hatched background', () => {
    const show = { north: true, west: true }
    return (
      <Wrapper>
        <PolarChart
          primary
          data={MOCK_DATA}
          show={show}
          sum
          hatched
        />
      </Wrapper>
    )
  })
