import React from 'react'
import PropTypes from 'prop-types'
import { withTheme } from 'styled-components'
import * as d3 from 'd3'


function computeCentroid(points) {
  const xs = points.map(p => p.x)
  const ys = points.map(p => p.y)
  const sumX = xs.reduce((acc, v) => acc + v, 0)
  const sumY = ys.reduce((acc, v) => acc + v, 0)
  return {
    x: sumX / points.length,
    y: sumY / points.length
  }
}

const DataPolygon = ({ theme, inverted, primary, scales, data, show, sum }) => {
  if (!data) return null

  const { north, east, south, west } = scales

  // Collect points
  let points = []
  if (show.north) {
    const item = data.north
    points.push({
      x: (east || west)(0),
      y: north(item.value),
      label: item.label,
      value: item.value
    })
  }
  if (show.east) {
    const item = data.east
    points.push({
      x: east(item.value),
      y: (north || south)(0),
      label: item.label,
      value: item.value
    })
  }
  if (show.south) {
    const item = data.south
    points.push({
      x: (west || east)(0),
      y: south(item.value),
      label: item.label,
      value: item.value
    })
  }
  if (show.west) {
    const item = data.west
    points.push({
      x: west(item.value),
      y: (south || north)(0),
      label: item.label,
      value: item.value
    })
  }

  // Add dummy point if less than 2 points in order to have enough for a polygon to fill
  if (points.length < 3) {
    points.push({
      x: (east || west)(0),
      y: (north || south)(0),
      isDummy: true
    })
  }

  let sumTextElm
  if (sum) {
    const centroid = computeCentroid(points)
    const sum = points.map(p => p.value || 0).reduce((acc, v) => acc + v, 0)
    if (sum > 0) {
      sumTextElm = (
        <text
          x={centroid.x}
          y={centroid.y}
          fill={theme.default}
          textAnchor="middle"
          alignmentBaseline="middle"
          fontFamily={theme.font2}
          fontSize="30px"
          fontWeight="300"
        >
          {Math.round(sum * 100)}%
        </text>
      )
    }
  }

  // Make polygon between points
  let polygonColor = inverted ? theme.inverted.one[0] : theme.one[0]
  if (primary) polygonColor = theme.color[1]
  let strokeColor = inverted ? theme.inverted.one[0] : theme.one[0]
  if (primary) strokeColor = theme.color[1]
  const firstPoint = points[0]
  const restPoints = points.slice(1, points.length)
  const path = d3.path()
  path.moveTo(firstPoint.x, firstPoint.y)
  restPoints.forEach(p => path.lineTo(p.x, p.y))
  path.closePath()
  const pathElm = (
    <path
      fill={polygonColor}
      fillOpacity="0.2"
      stroke={polygonColor}
      strokeWidth="2"
      d={path.toString()}
    />
  )

  // Make circles at points
  let realPoints = points.filter(p => !p.isDummy)
  const circleElm = realPoints.map((point, i) => (
    <circle
      key={i}
      cx={point.x}
      cy={point.y}
      r="5"
      stroke={strokeColor}
      strokeWidth={2}
      fill={inverted ? theme.inverted.two[0] : theme.two[0]}
    />
  ))

  return (
    <g>
      {pathElm}
      {sumTextElm}
      <g>{circleElm}</g>
    </g>
  )
}

const dataItemPropType = PropTypes.shape({
  name: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired
})

DataPolygon.propTypes = {
  scales: PropTypes.shape({
    north: PropTypes.func.isRequired,
    east: PropTypes.func.isRequired,
    south: PropTypes.func.isRequired,
    west: PropTypes.func.isRequired,
  }).isRequired,
  data: PropTypes.shape({
    north: dataItemPropType,
    east: dataItemPropType,
    south: dataItemPropType,
    west: dataItemPropType,
  }).isRequired,
  show: PropTypes.shape({
    north: PropTypes.bool,
    east: PropTypes.bool,
    south: PropTypes.bool,
    west: PropTypes.bool
  }),
  sum: PropTypes.bool,
  primary: PropTypes.bool,
  inverted: PropTypes.bool
}

DataPolygon.defaultProps = {
  sum: false,
  primary: false,
  inverted: false,
  show: {
    north: true,
    east: true,
    south: true,
    west: true
  }
}

export default withTheme(DataPolygon)
