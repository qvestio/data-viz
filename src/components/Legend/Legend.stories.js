import React from 'react'
import { storiesOf } from '@storybook/react'

import LegendSvg from './LegendSvg'
import { makeGroupColorScales } from '../../util/d3Utils'
import theme from '../theme'

const MOCK_DATA = [{
  groupId: '1',
  name: 'Nurses',
}, {
  groupId: '2',
  name: 'Doctors',
}, {
  groupId: '3',
  name: 'Experts',
}, {
  groupId: '4',
  name: 'Admins',
}, {
  groupId: '5',
  name: 'Someone with really long name',
}, {
  groupId: '6',
  name: 'Come on with those group names damn it',
}]

const MOCK_THEME_DATA = [{
  themeId: '549a8ff1-d9be-44bd-8bca-2428afabac9b',
  name: 'Procedures'
}, {
  themeId: '10514aec-3851-4db6-aed2-3643623b6886',
  name: 'Strategy'
}, {
  themeId: '8536028e-a3ef-4662-9ea2-ecff8c83a74a',
  name: 'Well-being'
}]

const groupColorScale = makeGroupColorScales(theme, MOCK_DATA.map(g => g.groupId)).primaryColors
const themeColorScale = makeGroupColorScales(theme, MOCK_THEME_DATA.map(g => g.themeId)).secondaryColors

storiesOf('Data Visualization/Legend', module)
  .add('Groups', () => {
    return (
      <LegendSvg groups={MOCK_DATA} colorScale={groupColorScale} />
    )
  })
  .add('Themes', () => {
    return (
      <LegendSvg inverted background variant="themes" themes={MOCK_THEME_DATA} colorScale={themeColorScale} />
    )
  })
