import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'


const List = styled.ul`
  padding: 0;
  margin: 4px 0 0 0;
  list-style-type: none;
  ${({ horizontal }) => horizontal && `
    display: flex;
    flex-wrap: wrap;
    & > * { margin-right: 10px; }
  `}
`

const ListItem = styled.li`
  display: flex;
  flex-direction: row;
  align-items: center;
  list-style-type: none;
  flex-wrap: nowrap;
  width: ${({ horizontal }) => horizontal ? 'auto' : '100%'};

  & > * {
    margin-right: 8px;
  }
  & > :nth-child(2) {
    flex: 1 0;
    min-width: 50px;
    text-overflow: ellipsis;
    white-space: nowrap;
    overflow: hidden;
  }
`

const ColorPreviewCircle = styled.div`
  background-color: ${({ color }) => color};
  border-radius: 50%;
  width: 8px;
  height: 8px;
`

const Text = styled.span`
    font-family: ${({ theme }) => theme.font1};
    font-weight: 500;
    font-size: 13px;
    line-height: 21px;
    color: ${({ theme }) => theme.one[0]};
    opacity: 1.0;
`

const MAX_ITEMS = 6

const Legend = ({ data, horizontal }) => {
  let truncateElement
  if (data.length > MAX_ITEMS) {
    truncateElement = (
      <ListItem key="-1">
        <Text variant="small" weight="light">...</Text>
      </ListItem>
    )
  }
  const displayedData = horizontal ? data : data.slice(0, MAX_ITEMS)
  return (
    <div>
      <List horizontal={horizontal}>
        {displayedData.map((node, i) => (
          <ListItem key={i} horizontal={horizontal}>
            <ColorPreviewCircle color={node.fill} />
            <Text variant="small" weight="light">{node.label}</Text>
          </ListItem>
        ))}
        {!horizontal && truncateElement}
      </List>
    </div>
  )
}

Legend.propTypes = {
  data: PropTypes.arrayOf(PropTypes.shape({
    label: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.element,
    ]).isRequired,
    fill: PropTypes.string.isRequired,
  })).isRequired,
  horizontal: PropTypes.bool,
}

Legend.defaultProps = {
  horizontal: false,
}

export default Legend
