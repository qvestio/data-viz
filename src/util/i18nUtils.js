import { addLocaleData } from 'react-intl'
import daMessages from '../lang/da.json'
import daLocaleData from 'react-intl/locale-data/da'

export function loadLocale(locale) {
  // TODO: When it becomes needed, make this fetch from some remote server (TMS? GCloud storage?)
  let messages = null
  if (locale !== 'en') {
    messages = daMessages
    addLocaleData(daLocaleData)
  }
  return messages
}
