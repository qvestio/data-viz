import * as d3 from 'd3'


/*
 Map group ids to colors in a consistent way given the same set of ids
 */
export function makeGroupColorScales(theme, ids) {
  // TODO: Sort based on created date like themes? (Results in less confusing editor UX with jumping colors)
  const sortedIds = [...ids].sort()

  // Primary color scale (id -> color)
  const primaryColors = d3.scaleOrdinal()
    .range(theme.data.primary)
    .domain(sortedIds)
  // Secondary color scale (id -> color)

  const secondaryColors = d3.scaleOrdinal()
    .range(theme.data.secondary)
    .domain(sortedIds)

  return { primaryColors, secondaryColors }
}

/*
  Map themes to colors in a consistent way
 */
export function makeThemeColorScales(styledTheme, themes) {
  // Prepare Ids sorted by created date
  themes = [...themes]
  themes = themes.sort((a, b) => a.createdAt - b.createdAt)
  const themeIds = themes.map(t => t.themeId)

  // Make scale
  return d3.scaleOrdinal()
    .range(styledTheme.data.secondary)
    .domain(themeIds)
}
